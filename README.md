# Machine Week

*Machines in a week*  
*It's easy, so to speak*   
*In minutia is mayhem*  

This week, you get to design a machine, and then build it, 'bring it online', and then do something with it. Exciting! There's a great deal of complexity here! I have done this a few times now<sup>1</sup>, and every time it's a new adventure.  

The type of machine you build (with your section) is your discretion. Besides making a straightforward 3-axis CNC Machine (a-la Shopbot, as documented in the following guide) - there are a few cool variations you can explore:

[5-axis CNC Machine](https://www.youtube.com/watch?v=CqePrbeAQoM)

[A Delta Machine (3-axis and neat kinematics)](https://www.youtube.com/watch?v=v9oeOYMRvuQ)

[Scara Arms](https://www.youtube.com/watch?v=3UF_lYx5qcY)

[Hot-Wire Cutters](https://www.youtube.com/watch?v=yKpq9FZMPqQ)

[Pick and Place](https://www.youtube.com/watch?v=CRSLbo_8nTQ)

[Lathes](https://www.youtube.com/watch?v=eipyve8xv24)

etc! weirdness encouraged

This document will serve as a guide for how to make a 3-axis machine. In linear time, I'm going to run through my design and fabrication processes, including links, resources and asides when relevant. You should read through it as a launching point for your design. As a default, you are free to use all of the resource here to replicate the machine, and develop an end effector of your own.

*!ALERT! ~ This is a design process ~ !ALERT!* so please bear with any ambiguities and nonlinearities. When possible, I will take asides to explain my reasoning, but overall, I hope to demystify CNC D&B<sup>2</sup> in fairly broad terms. Think of it as a style guide (?) more than direct instructions. Good luck, have fun!

### In this order, we will do:

#### Design:
 - Draw a Layout
 - Detail the Axis
 - Detail Interconnects

#### Manufacturing:
 - Do Material Layout in Rhino
 - Program CAM in Fusion
 - Do the Milling

#### Assembly:
 - Put it together!

#### Wiring
 - Plugs, Switches, Power, oh my!

#### Robot Talk
 - TinyG with a Big Heart (and second-order acceleration curves!)
 - Chilipeppr (haute browser interface)
 - GCode

#### End Effectors
 - Open Season: Design your own!

# Design

## In Short

We're going to take these axis from [Jens](https://github.com/fellesverkstedet/fabricatable-machines)

![linear axis](images/linear-axis.jpg)

![linear axis](images/rotary-axis.jpg)

And compose them somehow a-la [Nadya](http://mtm.cba.mit.edu/machines/science/)

![nadya-compose-axis](images/nadya-compose-axis.png)

## Layout 

First thing, you'll want to get a hang of what rough sizes / shapes / orientations your machine is going to have. In this case, I'm interested in designing something of an 'everything machine'. I.E it should be useful for a few different processes: 3D Printing, CNC Milling, Flat-Sheet Cutting (a-la the ZUND), and (maybe) eventually Laser Cutting. Normally I would not advise this<sup>3</sup>, but here we are.  

I'm going to aim at a roundabout bed-size<sup>4</sup> of 12x24"<sup>5</sup> - largely this just feels like a happy medium between large format and small format work. It's a fairly common size for sheet stock, or at least bigger sheet stock can be broken down into these sizes with minimum work. In Europe, sheets also commonly come in 1250x2500mm stock - a factor of 1.025 over the NA 4'x8' standard. I'm going to add 1" to each of these dimensions to account for that, and for general design-fudge-space, and for fixturing. I feel like 5"<sup>6</sup>is a great Z-travel value - this will handle lots of stock, and lots of tools (in milling) - also, this is relative movement, and I plan on making the overall bed height / end-effector mount locations somewhat adjustable.   

SO: 13x25x5" moving area.  

W/R/T Layout, there are a number of permutations of how to go about adding axis together in order to get 3D motion. I'd like to cover a few of these in examples, and I'll leave a TODO here: - bridgeport x-on-y with beefy-z style, shopbot and laser 'H' machine w/ dual y-drive (note laser BED moves, not head), omax and fablight 'drafting-square' hella-stiffness gantries, ultimaker t-config, corexy, flexural stages. I really like [this machine](http://archive.fabacademy.org/2017/fablabverket/students/100/web/projects/diy_cnc/index.html) developed by one of Jens' students. I should explain why<sup>7</sup>. OK, enough talk - let's see an example of a machine layout - 

I tend to 'work out' from the Z-axis, towards the edges - this way I can keep track of where I need extra offsets (length of travel != length of gantry). Here's the layout with the Z-and-X axis group moved around to the extents.

![machine layout extents](images/layout-extents.jpg)

And the layout as representative of a real machine...

![machine layout](images/layout-machine.jpg)

*NONLINEARITY ALERT* You can tell I already have some sense of what the details in my axis system are like, which has informed (in a big way) how I laid the rest of the machine out. HOWEVER - as this stage, I took no time to carefully align things, set thicknesses, etc. I was simply trying to get a *general sense* of what-goes-where. This will inform my next spiral, where I detail axis. 

![machine layout extents](images/design-space-cone.png)

Good design is kind of like this - you are starting far away from your desired goal, and in limited time, you approach *the destination*. Deviations you make early on have big effect on your final location - even though this is when you have the most limited amount of information. It's important, during the early stages, to properly explore as much 'design space' as possible - this way you are better off later on - sometimes in categorically better or worse positions. SO: don't jump too early, be curious, be cautious, think as carefully as you can as you make these initial decisions. 

Also of note: Slocum<sup>8</sup> has a lot of great <a href="fundesign">resources for machine design</a> that are totally approachable, pretty weird, and occasionally funny. He advocates (rightfully so) very strongly for doing lots of back-of-the-envelope guestimation at the early stage: how much torque can I expect motors to require? What are the Free Body Diagrams like? What kind of bits & parts are available? How much moving mass is too much moving mass? etc. Spreadsheets are mundane, but they will serve you well - making later stages into oodles of fun.

## Parametric Axis

Jens Dyvik is on some [wonderful machine building sprials (link!)](https://github.com/fellesverkstedet/fabricatable-machines) and we're going to put them to work this week. In particular, the [chamferrail system](https://github.com/fellesverkstedet/fabricatable-machines/tree/master/chamferrail). Linear Axis are like the bread & butter of Mechanical Engineering - and making a good one is more subtle than you would imagine. Jens has developed some great axis (rotary too!) and so we are borroging from his toolkit this week. Take a look at his documentation to get an overview of the machines! 

I'm using his Chamferrail Generator in Rhino and Grasshopper - included in this repo under /cad/axis-generator/ and in [Jen's Repo here](https://github.com/fellesverkstedet/fabricatable-machines/tree/master/humphrito-medium-format-cnc). 

Links to these files are under /cad/axis-generator/ - you can open the Rhino and Grasshopper file there TODO: Link Grasshopper Tutorials and get to work inputting your parameters. 

You can make a linear axis:

![linear axis](images/linear-axis.jpg)

Or a rotary axis:

![linear axis](images/rotary-axis.jpg)

Use Grasshopper to adjust the parameters - you'll find them all on the left. There's a huge swath to get through, try experimenting so that you understand the different parameters. Critically, adjust Axis Length, Axis Width, and Axis Type (Rotary or Linear). The Motor Size & Material Thicknesses are set already for the materials we have on hand for machine week, and the Tooth Size variables are set up for the milling capabilities we have in our shops.

Once I have my parameters set up, I use the 'bake' command to pull the geometry out of grasshopper and into Rhino.

![generating the x axis](images/generate-x-axis-bake.jpg)

I'm going to do this for each of my axis: X, Y and Z - and then bring those into another Rhino File so that I can start composing them into a machine.

## Detail: Layout

OK, I have my axis, and now I'm going to fine-tune my layout.

![generating all axis](images/generate-all-axis.jpg)

Critically, I coordinated mounting holes (there's an option for 'custom hole pattern' in the Glideblock Geometry Section) so that my X and Z axis would comfortably overlap:

![coordination x-z axis](images/coordinate-x-z-axis.jpg)

Now I'm going to bring these into my layout:

![begin-layout](images/begin-layout.jpg)

What I'll do now is compose axis 'from the action-out' ... i.e. I'm going to set up my Z->X relationship up, and then my X->Y relationship up. So, if you have a Delta Machine, Scara Arm, etc - I recommend starting detail design from the 'last point' in your motion system - probably the point whose coordinates (movement) matters the most. This is my Z->X Axis Relationship:

![layout-z](images/layout-z.jpg)

Now is when we start deviating from the parametric axis. I'm going to make on major concession: my Y axis (the long ones) I have split, and I'm going to put one side of the rail on either, and link them with a rigid member along the x axis... I'm also going to bring one of the adjustable-sides of the chamferrail across<sup>9</sup>.

![layout-x-y](images/layout-x-y.jpg)

I'm feeling ready to start 'boxing it out' - i.e. adding some structure to these so-far so-flappy axis. Here we go:

![layout-ready](images/layout-ready.jpg)

And the X-Axis... Stiffness is king! But weight is not your friend...

Starting by linking the two Y-Axis Gantries with a block - this way, I can comfortably constrain these gantries relative eachother - since they are operating on a split rail. TODO: Kinematic Jams  

![pre-box-y](images/layout-pre-box-y.jpg)

I was having a pretty hard time ironing this out. 'Annealing' the ~ design-space ~ . I printed a screenshot from Rhino<sup>10</sup> and tried doodling. Design Education gets a +1 pt for this moment... I figured it out pretty quickly! Actually on paper it seemed really obvious. Brains, weird!

![pre-box-y-sketch](images/layout-box-y-sketch.jpg)

This is satisfying: I have a squared-out structure that I feel good about, it's not *too* heavy...

![pre-box-y-together](images/layout-box-y-together.jpg)

And putting the X on the Y axis is semi-deconstructable / easy-ish to assemble: 

![pre-box-y-mod](images/layout-box-y-mod.jpg)

OK. I feel good about this, I'll throw in a few *bonus* features (like, a bed could be nice?) and call the blocking-out section done.

Here's the bed. I make no claims to elegance, but this will make it easy to load in a sheet of spoilboard. I am largely trying to avoid cutting *so* much plastic, so these links are thin and sparse.

![layout-bed](images/layout-bed.jpg)

## Detail: Joinery

Now I need to rip it all together. In the fullness of time, all of these links become snap-fit, but for now I am going to tab-and-screw it together. Before getting deeper into it, I wanted to check the hole-and-pocket sizes from my time in Jens' tool agains the plastic-specific hardware I'm planning on using.

![detail-hardware](images/detail-hardware.jpg)

This is a No.8 Plastic Threadforming Screw @ 3/4" long. In machine design, hardware is no joke! For plastics I always choose a big beefy thread - [these](https://www.mcmaster.com/#96001a268/=1a4r2dm) are excellent, with only 16 threads / inch. This means that I'm much less likely to shear the threads out of my material (which is insanely soft relative the metals that Mx size Socket Head Capscrews are designed for). Additionally, washers often seem secondary, but are REALLY important. Here's a 'shear cone' drawing w/ and w/o a washer.

TODO: Shear Cones, w/r/t Slocum

Basically, you really want to distribute your load across a larger swath of material. [Here](https://www.mcmaster.com/#92141a009/=1a4r3kw) is the washer, just a regular No.8. We'll ask all section heads to get these hardwares on hand as well, for all of your plastic assembly joy. * They also use Torx Heads - another one of my favourite things.

Strategy for adding Holes in Rhino (use Boolean Difference):

![detail-holes](images/detail-holes.jpg)

Adding a trough in the frame so that the XY Gantry can slide on / off (subassemblies!) 

![detail-ygantry-slidethru](images/detail-ygantry-slidethru.jpg)

OK: I'm all lego-d out on the bottom section:

![detail-bottom-lego](images/detail-bottom-lego.jpg)

Detailing the top section is similar. Basically, I build little 'blocks' that I'll want to add / subtract from other objects (tabs, holes) and I array those about, adding and subtracting... It's a painful process, to be honest, but I actually think (on one-shot) it's a bit faster than adding a similar amount of information to a parametric model - short of scripting. Of course, it's not parametric, which can be a *huge* drag, esp. in these cases where, say, material may arrive and measure 9.7mm in thickness as opposed to 9.5mm (which, by the way, is what I used here). 

Here's the back of the X Gantry:

![detail-x-lego](images/detail-x-lego.jpg)

And the whole X / Y / Z Carriage:

![detail-x-y-z-lego](images/detail-x-y-z-lego.jpg)

And a color coded machine! These primaries are aggressive :|

![detail-x-y-z-lego](images/detail-x-y-z-colour.jpg)

Talk about a CAD Marathon! Some notes:
 - Not even sure this would have gone faster in Parametric CAD. HOWEVER - if I had to change anything, I would make time back in maybe one shot.
 - That's a lot of fasteners! I don't even want to count. I'm going to estimate 100 ? A real count gives me 140. A bit of a bummer, these screws are $10 / 50. 

# Manufacturing

## CAM: 

Once I'm ready to do some manufacturing, I start by grouping solids by material:

![cam-material-groups](images/cam-material-groups.jpg)

And then laying them out in their respective sheets. Here, I'm using a 4x4' x 3/8" thick sheet of HDPE for 'most' of the machine, and I'm using a 12x24" sheet of Delrin for the guideblocks (slippery things). Additionally, I've got a 3/4" 

![cam-material-sheets](images/cam-material-sheets.jpg)

Then I brought this into Fusion to do some CAM. From Rhino, select the geometry for one material and Export as a *.step* file. Then you can upload this into Fusion.

![cam-fusion-setup](images/cam-fusion-setup.jpg)

* careful with excessive import-cycles - i.e. the STEPs I brought in were slightly degraded in sections, I had to be careful when selecting contours to CAM.
* you can find my Fusion files under [machineweek/fab/fusion](https://gitlab.cba.mit.edu/jakeread/machineweek/tree/master/fab/fusion)

Now, I'm not going to try to relay exactly what I did. CAM is an art-ish. Here's a basic breakdown of the steps I took:

- Mill all of the Holes First
 - Use those holes to fixture parts to the bed
 - This is critical, because it's important the material is consistently flat w/r/t the bed
 - This means you can avoid using tabs
 - Use a Bore Toolpath to generate Holes

- Clear Pockets and Faces
 - I.E. Countersunk Screws, Holes for Indexing Tabs
 - Use a 2D Pocket or Adaptive Clearing Toolpath
 - Use 'multiple depths' to generate step-downs when the pockets are deep.

- Contour to Cutout
 - Cut sheets now that you're fixtured
 - Parts w/o holes for fixturing should be 'tabbed' to prevent them moving about as they are cut
 - Use 'multiple depths'

- Remove Excess Stock
 - Now you can peel the 'rest' of the material away. 

- Contour Details 
 - Use a Contour to detail the Teeth, using a gentle stepover.
 - Pocket beforehand to remove the majority of the material prior to detailing

- Chamfering
 - Start a sketch *on* the chamfered surface
 - Use this plane / sketch to draw a line parallel to the chamfer, offset by 1.25mm
 - Use this line and a Trace toolpath to drag the chamfering bit along
 - Tangential Offset will allow you to 'clear' the edges well
 - Multiple Depths (z-offset) means you won't be taking a huge cut
 - Use a gentle stepdown so that the machine makes a very clean cut - this will improve your machine slipperyness!

- Feeds and Speeds
 - [CBA Feeds and Speeds Calculator](https://pub.pages.cba.mit.edu/feed_speeds/)

#### HDPE: 

Type | Flutes | Diameter | What For | Feed, XY (IPM) | Feed, Plunge (IPM) | Spindle Speed (RPM)
--- | --- | --- | --- | --- | --- | ---
Endmill | 1 | 1/8" | Most Details, Holes | 55 | 25 | 13500
Endmill | 1 | 1/4" | Cutouts, Grunt | 55 | 25 | 7000 
Endmill | 2 | 1/16" | Pinion Detail | 75 | 25 | 20000
Chamfer Mill | 2 | 1/2" | Rail Edges | 100 | 50 | 5000 

#### Acetal: 

Type | Flutes | Diameter | What For | Feed, XY (IPM) | Feed, Plunge (IPM) | Spindle Speed (RPM)
--- | --- | --- | --- | --- | --- | ---
Endmill | 1 | 1/8" | Most Details, Holes | 55 | 25 | 13500
Endmill | 1 | 1/4" | Cutouts, Grunt | 55 | 25 | 7000 
Endmill | 2 | 1/16" | Pinion Detail | 75 | 25 | 20000
Chamfer Mill | 2 | 1/2" | Rail Edges | 100 | 50 | 5000 

#### Fly Cutting MDF:

Type | Flutes | Diameter | What For | Feed, XY (IPM) | Feed, Plunge (IPM) | Spindle Speed (RPM)
--- | --- | --- | --- | --- | --- | ---
Fly Cutter* | 2 | 2 & 3/8" | Facing the Bed | 130 | 50 | 6500

* I used a 1.5" stepover, and ~ 0.03" stepdown. I set the facing pattern up to only cut on the 'climb side' of the bit - that was pretty critical. I think it's possible to be more aggressive - the bit we have at the CBA is quite dull. The job ran about ~50 minutes. 

## Machining

### Prepping Your (already-existing) Machine

*CRITICAL NOTE*
These linear axis **absolutely** require you to face off the spoilboard on your milling machine. If the XY Plane that your material rests on is not truly parallel to the XY Plane that your gantry moves along<sup>11</sup> you will have axis whose chamfered-edges vary in width. This will cause some areas on the gantry to jam up, and others to be loose!
*END CRITICAL NOTE*

First thing, I surfaced the bed. We have this big gnarly cutter at the CBA: 

![fly-cutter](images/fly-cutter.jpg)

I made a tool for this in Fusion (TODO: include in table), it's included in the table below.

![fly-cutter-fusion](images/fly-cutter-fusion.jpg)

And I ripped out a 'face milling' toolpath:

![face-mill-fusion](images/face-mill-fusion.jpg)

Then I got on the shopbot, and ran the job! I set the Z (in the program, it just faces along Z0.0) such that the machine thought 0.0 was about 0.1" below the surface. I had to run the job twice, bringing it down by 0.1" each time, to get all of the low spots. Nice circular turnaround courtesy of Fusion 360 CAM: 

![the-face-milling](images/doing-the-face-milling.gif)

*NOTE* objects in GIF appear faster than real life *NOTE*  

I ended up doing this a second time, with a better toolpath and a bigger bite.  

![milling-second-face](images/milling-second-face.jpg)

### Doing the Milling

![milling teeth](images/milling-teeth.gif)

![milling chamfers](images/milling-chamfer.gif)

These are the chips you want. Machining plastic should be impressively smooth, silent and buttery.

![the-chips](images/the-chips-you-want.jpg)

This is the chip-fountain you want:

![the-fountain](images/the-fountain-you-want.gif)

In time (est. 1 full day, at least!), I finished all the milling.

![good-job](images/good-job.gif)

# Assembly

So I get all of the bits together, and clean the edges up where I need to with a Deburring Tool & regular old knife.
 
![assembly-debur](images/assembly-debur.jpg)

I started with the X/Z Block, tapping the guideblock's back-side. With tapping plastics, I normally just chuck the tap in a drill. Careful to go in straight, though!

![assembly-tap](images/assembly-tap.jpg)

There's a subtlety to putting the glideblocks together. The M5 SHCS goes through three layers - this is not common / encouraged in 'precision' machine design, because there is some uncertainty about how exactly the layers line up.<sup>12</sup> Critically, the middle block (the one with the chamfer) is a *very slight* slip-fit (5.1mm), the outer block is a large slip (5.4mm) and the final block is threaded. ALSO: Don't forget a washer!

![assembly-guideblock-hardware-stack](images/assembly-hardware-stack.jpg)

I got two sides together to test it out:

![assembly-slide-test](images/assembly-ways-test.jpg)

There's a reason one side of the glideblocks is mounted on a slot - this way you can adjust the spacing between the blocks - which is important! This way we can slide the gantry over the rail and then *tune* the stiffness.

![assembly-gantry-adjust](images/assembly-gantry-adjust.gif)

OK, there's the block, ish

![assembly-xz-no-motors](images/assembly-xz-no-motors.jpg)

Now I'm going to get some Set Screws tapped into the Pinions, and try mounting motors to this block. M4 Tap Drill Size is 3.3mm<sup>13</sup>, so a #30, so a 0.1289", etc

![assembly-tapdrill-select](images/assembly-drill-select.gif)

Same drill here for the hole-tapping<sup>14</sup>. This time, M4. I put the pinions in a vise, using the index markers to drill perpendicular to the shaft.

To get the pinion teeth in the right spot w/r/t the rack, there should be about 24mm between the motor flange and the top of the pinion. Check this against the actual machine. Also, make sure one of the set screws is aligned with the 'D' cutout on the motor shaft.<sup>15</sup>

![assembly-pinion-d](images/assembly-pinion-d.jpg)

Now I get the motor on the motor-plate - countersinking the M5 FHCS<sup>16</sup> into the plate. This way, the plate can ride flush against the rest of the gantry. 

![assembly-motor-plate](images/assembly-motor-plate.jpg)

And mount that on your gantry. I used those plastic self-tapping screws here. AND I left them a bit loose. You'll notice that the motor slides, also, relative the gantry:

![assembly-motor-slid](images/assembly-motor-slide.gif)

Adjustment: it's a theme.

Now, I got a good look at my pinion -> rack engagement. 

![assembly-pinion-check](images/assembly-pinion-check.jpg)

Feeling good about this, so I celebrated with a [knoll](https://www.youtube.com/watch?v=s-CTkbHnpNQ). Nothing like it.

![assembly-knoll](images/assembly-knoll.jpg)

To get the rest together, I'm using a 1/8" drill diameter to pre-drill for these plastic screws, and then driving them in with a T20 bit. In addition to this, I don't screw them all down 'in sequence' - I add screws kind of sporadically to the structure - this way I don't pre-stress it in any particular direction. And make sure to use washers!

* a note on rigidity / 'deep' structures - here's the frame w/o any boxiness:

![assembly-flap](images/assembly-flap.gif)

The rest of it is pre-drilling holes and fastening with Plastic Thread Screws

![assembly-fasteners](images/assembly-fasteners.jpg)

Now draw the rest of the owl

![assembly-owl](images/assembly-owl.jpg)

# Wiring

I made no concessions for wiring while I was designing - if you're going to 'rev' the machine I made, please do this! If not, it pays to do a tiny bit of planning for your wiring. Keep in ind wires have to move. This part tends to be best detailed in-situ... My strategy is to drill holes where I want to mount wires, and loop zip-ties around them, in bundles, there.

I got lucky and had accidentally spaced my bed-rails at the pitch of some mount holes on the Power Supply. Rad.

I mounted those, and the TinyG. To bring power to the PSU, I cut up a DIN connector and plugged the GND, Line and Neutral lines in.

The TinyG gets power here - polarity should be obvious. You should be being safe and not turning anything <strike>off</strike> on yet! Careful with the TinyG polarity - it will die if you reverse these.

Next, I drilled some holes in the chassis for my motor wires. I also found this rad 4-conductor cable in the basement at the CBA, I'll use that to route out to my motors.

![assembly-ee](images/wiring-ee.jpg)

Next up is wiring the stepper motors. There's not really a standard for colors, as far as I can tell - or at least it's always worth checking. Stepper Motors are two coils: 

![stepper-wiring](images/doc-stepper-wires.jpg)

And you'll see on the TinyG 'A1, A2, B1, B2' - it's not important which direction these are, just that you put the pairs together. In order to figure out which wires belong to which coils, there are two ways to test: (1) get a multimeter, and do a beep-test. (2) when you touch a coil pair together, the motor will be much harder to turn. Pretty neat. 

So, plug those wires in, run some leads out to your motors.

Also, [here's a link to your stepper motor](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-09deg-126nm-1784ozin-28a-25v-57x57x56mm-4-wires-23hm22-2804s.html) where you can find a datasheet.

![wiring-stepper-leads](images/wiring-stepper-leads.jpg)

I also took some time to make the cables 'happy' - i.e strain releived. Next up, I'm ready to sort through the firmware / communications.

# Robot Talk

## Configuring TinyG

TinyG Doc is [here](https://github.com/synthetos/TinyG/wiki). You can power the TinyG and plug in your USB - you should see it appear on your serialport list. First thing, you'll want to make sure TinyG has the latest firmware flashed - do that with the [TinyG Updater App](https://github.com/synthetos/TinyG/wiki/TinyG-TG-Updater-App). 

## Chilipepper

Chilipeppr is a browser tool for machine tools. It is familiar with TinyG it lives [here](http://chilipeppr.com/tinyg). Documentation is [here](https://github.com/synthetos/TinyG/wiki/Chilipeppr). 

To talk to your serial port, Chilipeppr uses a Serial Port Json Server - you can find a download link in the bottom-right. 

![chilipeppr-splash](images/chilipeppr-splash.jpg)

I'll let you get into that doc on your own, as it's much better than what I can write in the next few minutes. If you're jumping ahead, the configuration you need is in the tiny gear-icon on the right side.

![chilipeppr-config](images/chilipeppr-config.jpg)

Here you can set motors up - and in the 'axis' tab, you can set things like max. speed, acceleration (max jerk) etc. First thing, you'll want to get Motor setup in order, and make sure your steps / rev is correct - so that when you tell the machine to move 1mm, it makes as many steps as are in 1mm. Here it is for the 'default' axis we are using:

- 45.045 mm/rev
- eighth microstepping
- make sure y axis are flipped relative eachother

So, you should be just about ready to write gcode, or send gcode to the machine. As for end effectors, you're on your own for config. Apologies.

## Gcode Basics

[G-Code](https://en.wikipedia.org/wiki/G-code) is an ancient (1950s?) method for talking to CNC Machines. Most everything in the CBA shop, at it's heart, is a G-Code interpreter - meaning, it reads lines of G-Code and moves around accordingly. This is great for most manufacturing, but horrible if you want to do any kind of interaction, programmatic moves, etc. However, G-Code persists, so it's OK to get to know it. 

If you open a Serial Terminal connected to TinyG, you can type these in directly to jog / test etc. It's also straightforward to write small files for testing.

Here's a sample:

```
%
; 1001 ; comments are behind semicolons
; T4 D=6.35 CR=0 - ZMIN=-10.5 - flat end mill
G90 ; mode: set absolute positioning
G17 ; set xy plane
G21 ; use mm

; Face1
T4 ; use tool  #4
S9500 M3 ; M3: turn Spindle On, S9500: RPM
M106 ; Fan on
G0 X-2.821 Y-7.571 F1000 ; G0: Rapid Move (i.e. jogging)
G0 Z15
G1 Z5
G1 Z0.635 F250 ; G1: Linear Move (i.e. machining)
G19 G3 Y-6.936 Z0 J0.635 ; G19: set plane xy for G3: counter-clockwise arc
G1 Y-4
G1 Y39
G17 G2 X1.485 I2.153 ; clockwise arc
```

Critically, G-Code is 'Modal' - so if I set a feedrate during one move, it will make the next move at the same rate. You only need to know a few of these to start out:

```
G0 X<pos> Y<pos> Z<pos> F<feedrate> ; rapid - use for jogging the machine around
G92 X<pos> Y<pos> Z<pos> ; coordinate offset - tells the machine that this input is it's current position
G28 ; home axis - only works if you have homing switches!
```

# Moving Along

*ooh ahh*

![config-move](images/config-move.gif)

![pen-plotter](images/pen-plotter.jpg)

# Bill of Materials

The total cost for one machine is ~ **$523 USD**, although the cost of hardware has not been broken up per-unit of hardware (instead, the minimum number of 'packages' is used). The largest expense is in the TinyG controller, which we hope to replace with modular control, in the Motors, and the HDPE and Delrin. This cost scales considerably with aluminum.

The total cost for tooling is ~ **$194 USD**. This does not include the cost of a shopbot...

## Controller

 - [TinyG](https://github.com/synthetos/TinyG/wiki) 
 - [buy on Adafruit](https://www.adafruit.com/product/1749) ($165)

## Power Supply
 - [24v 14.6A switching PSU](https://www.omc-stepperonline.com/power-supply/350w-24v-146a-115230v-switching-power-supply-stepper-motor-cnc-router-kits-s-350-24.html) ($23.96)

## Motors
 - [4x NEMA 23 57x57x56 w/ 6.32mm Shaft w/ D w/ 400 Steps / Rev](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-09deg-126nm-1784ozin-28a-25v-57x57x56mm-4-wires-23hm22-2804s.html) (4x $18.85)

## Tooling
 - 1/8" O-Cutter Upcut - https://www.bhid.com/itemdetail/ONSCUT%2065-013 Onsrud PN 65-013 ($36.46)
 - 1/4" O-Cutter Upcut - https://www.bhid.com/itemdetail/ONSCUT%2065-023 Onsrud PN 65-023 ($48.70)
 - 1/2" 90deg Chamfer Endmill - https://www.bhid.com/itemdetail/NIACUT%20N76595 ($66.44)
 - 1/16" O-Cutter Upcut - https://www.bhid.com/itemdetail/HARVEY%2051162 ($30.66)
 - M5 Tap - 8305A56 ($6.67)
 - M4 Tap - 26565A52 ($5.85)

## Hardware

 - 140x Plastic Screws, No.8 x 3/4" 96001A326 (3x $13.00)
 - 140x Washers for Plastic Screws 92141A009 (2x $2.00)
 - FHCS M5x22 (Motor Mounting) 92125A215 ($9.50)
 - SHCS M5x18 91292A127 ($8.01)
 - SHCS M5x35 (Rails Mounting) 91292A193 ($5.53)
 - Washers M5 - 93475A240 ($2.57)
 - 12x Set Screws M4x8 - 92015A113 ($8.39)

## Material
 - Rails / Structure etc: 3/8" x 48x48" HDPE - 8619K437 ($90.98)
 - Glide Blocks: 3/8" x 12x24" Acetal (Delrin) Sheet - 8573K37 ($74.71)
 - Pinions: 0.75" x 2x6" Acetal (Delrin) Bar - 8739K44 ($16.55)

## End Effector Showdown
 - A Router 
 - A Laser Diode (haute)
 - A 3D Print Head (also haute)
  - Heated Bed ?
 - Ceramic Printer
 - Pick-and-place-ish
 - Drawing
 - Camera / Scanning
 - Cutting

# Footnotes

1. [Five Axis](http://ekswhyzee.com/index.php/project/tinyfive/), [Metal Laser Cutter](http://ekswhyzee.com/index.php/project/mako/), [and here](http://3dfablight.com/), [Dual Head 3D Printer](http://openassemblies.com/index.php/fdm4md/), and [Ongoing Robot Arm Adventure](http://openassemblies.com/index.php/rsea/)   
2. Design and Build
3. Things work very well when they are designed to do only one-thing. For example, vise grips *will* turn just about anything, but no one would say they are *good* at turning *anything*. A building designed for Helsinki may not make so much sense in Dubai. In another example, a laser cutter has a motion system that is optimized for speed, and takes advantage of the fact that it has very little mass to move around (a few mirrors) in order to carry through on this optimization. A milling machine is engineered for stiffness, and trades speed for the mass required to carry through on that optimization. In trying to have one motion system do all of these things, we'll go a little 'soft' in the middle, but we'll also be able to offer a lot of variety in a single system.
4. Relative Scaling: 10^4 of length scale is a common machine, 10^6 is good - lookup slocum ?
5. ~ 305x610mm 
6. ~ 127mm
7. So I want an H-style layout, because I want to keep the machine small relative it's total work area. One of the biggest drawbacks with an H-machine is that the two sides of the Y-axis are not always set up parallel. The result is what's called 'racking' - i.e. imagine opening a screen door, and the top or bottom exhibits more friction - the 'jam' that this causes happens in CNC Machines as well. A drawing. By cutting both Y-axis rails out of the same 'frame', Jakob gets around this issue - the parallelness of the two rails is a mirror of the parallelness of the machine which cut them. It makes it a bit bulletproof to novice assemblers. He has also done a really good job of keeping the X-axis loads really close to the Y-axis rails (so, a small structural loop). 
8. Slocum Link
9. Adjusting one side, fixturing the other.
10. TODO: note on printing from rhino, to scale
11. TODO: imperfections note
12. TODO aside, motor concentricity
13. TODO: Link Tap Drill Chart
14. Pun Alert
15. note on bearing->load distance, slocum, w/r/t pinion hanging way out
16. For Flat Head

# To Do

- Link to Fusion 360 w/ cleaned Cam files
- Finish / Fix Loose Footnotes
- Link extra resources: Dan Gelbart, Slocum etc
- Chat aboot resolution vs. accuracy, global / local etc
- w/r/t *True Level* - Everything is crooked... lambs to the cosmic slaughter
- s/o to Hector & Jakob